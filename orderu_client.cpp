#include "orderu_client.h"
#include "global/serializer.h"

void orderU::orderu_client::receiveMessage(connection_ptr conn, const orderU::message& _message, orderU::message** answer)
{
    LOG("IN RECEIVE HANDLER");
	switch (_message.id)
	{
	case MessageType::SMSG_TABLE_LIST:
	{
		auto tables = serializer::deserialize<std::vector<std::shared_ptr<table>>>(_message.data);
		getTableManager()->set_list(tables);
		break;
	}
	case MessageType::SMSG_ITEM_LIST:
	{
		auto list = serializer::deserialize < std::vector<std::shared_ptr<item>>>(_message.data);
		getItemManager()->set_list(list);
		break;
	}
	case MessageType::SMSG_WAITER:
	{
		auto _w = serializer::deserialize <waiter_client>(_message.data);
		_waiter = _w;
		break;
	}
	case MessageType::SMSG_BILLS:
	{
		auto list = serializer::deserialize < std::vector<std::shared_ptr<bill>>>(_message.data);
		getBillManager()->set_list(list);
		break;
	}
	case MessageType::SMSG_HELLO:
	{
		message msg;
		msg.id = CMSG_HELLO;
		_service->write(msg, [](message) {});
		break;
	}
	case SMSG_ADD_ITEM:
	{
		std::shared_ptr<item> i = serializer::deserialize<std::shared_ptr<item>>(_message.data);
		_im->add(i);
		break;
	}
	case SMSG_ADD_BILL:
	{
		std::shared_ptr<bill> i = serializer::deserialize<std::shared_ptr<bill>>(_message.data);
		_bm->add(i);
		break;
	}
	case SMSG_ADD_TABLE:
	{
		std::shared_ptr<table> i = serializer::deserialize<std::shared_ptr<table>>(_message.data);
		_tm->add(i);
		break;
	}
	case SMSG_REM_TABLE:
	{
		std::shared_ptr<table> i = serializer::deserialize<std::shared_ptr<table>>(_message.data);
		_tm->removeItemById(i->getId());
		break;
	}
	case SMSG_REM_ITEM:
	{
		std::shared_ptr<item> i = serializer::deserialize<std::shared_ptr<item>>(_message.data);
		_im->removeItemById(i->getId());
		break;
	}
	case SMSG_REM_BILL:
	{
		std::shared_ptr<bill> i = serializer::deserialize<std::shared_ptr<bill>>(_message.data);
		_bm->removeItemById(i->getId());
		break;
	}
	case SMSG_UPD_TABLE:
	{
		std::shared_ptr<table> i = serializer::deserialize<std::shared_ptr<table>>(_message.data);
		_tm->update(i);
		break;
	}
	case SMSG_UPD_ITEM:
	{
		std::shared_ptr<item> i = serializer::deserialize<std::shared_ptr<item>>(_message.data);
        _im->update(i);
		break;
	}
	case SMSG_UPD_BILL:
	{
		std::shared_ptr<bill> i = serializer::deserialize<std::shared_ptr<bill>>(_message.data);
        _bm->update(i);
		break;
	}
	default:
		break;
	}
}
//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __ITEM_H__
#define __ITEM_H__

#include <string>
#include <vector>
#include <memory>

#include "data_interface.h"

namespace orderU {

	class item_category : public data_interface
	{
	public:
		item_category(std::string name) : data_interface(name) {}
		item_category() :data_interface() {}

	};

	/// Structure to hold information about a single stock.
	class item : public data_interface
	{
		protected:
		float _price;
		std::shared_ptr< item_category> _category;

	public:

		item(std::string name, float price, std::shared_ptr< item_category> cat)
			: data_interface(name), _price(price), _category(cat)
		{
		}
		item(std::string name, float price)
			: data_interface(name), _price(price)
		{
		}

		item() : data_interface(){}

		~item(){}

		template <typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar & boost::serialization::base_object<data_interface>(*this);
			ar& _price;
			ar& _category;
		}

		float getPrice() const {
			return _price;
		}

		void setPrice(float price) {
			_price = price;
		}

		auto& getCategory() {
			return _category;
		}

		void setCategory(std::shared_ptr<item_category>& cat)
		{
			_category = cat;
		}


	};

} // namespace orderU

#endif // __ITEM_H__
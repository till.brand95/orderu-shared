//
// waiter_defines.h
// ~~~~~~~~~
//
// Copyright (c) 2019 Till Brand
//
//

#ifndef __WAITER_DEFINES_H__
#define __WAITER_DEFINES_H__


namespace orderU 
{
	enum waiter_role
	{
		WR_NONE = 0,
		WAITER = 1,
		ADMIN = 2,
	};

	enum user_permission
	{
		UR_NONE = 100,
		ITEM = 101,
		ORDER = 102,
		BILL = 103,
		TABLE = 104,
		USER = 105,
	};

	enum permission_type
	{
		PT_NONE = 0,
		READ = 1,
		UPDATE = 10,
		REMOVE = 100,
		ALL = READ | UPDATE | REMOVE,
	};

	static std::map<waiter_role, std::map<user_permission, permission_type>> normal_permissions =
	{ 
		{
			waiter_role::WAITER,
			{
				{user_permission::ORDER, permission_type::ALL},
				{user_permission::BILL, permission_type::ALL},
				{user_permission::TABLE, permission_type::ALL},
				{user_permission::ITEM, permission_type::READ},
				{user_permission::USER, permission_type::PT_NONE}
			}
		},
		{
			waiter_role::WR_NONE,
			{
					{user_permission::ORDER, permission_type::PT_NONE},
					{user_permission::BILL, permission_type::PT_NONE},
					{user_permission::TABLE, permission_type::PT_NONE},
					{user_permission::ITEM, permission_type::PT_NONE},
					{user_permission::USER, permission_type::PT_NONE}
			}
		},
		{
			waiter_role::ADMIN,
			{
				{user_permission::ORDER, permission_type::ALL},
				{user_permission::BILL, permission_type::ALL},
				{user_permission::TABLE, permission_type::ALL},
				{user_permission::ITEM, permission_type::ALL},
				{user_permission::USER, permission_type::ALL},
			}
		},
	};
} // namespace orderU

#endif // __WAITER_H__
//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __WAITER_H__
#define __WAITER_H__

#include <string>
#include <vector>
#include <memory>


#include "waiter_defines.h"
#include "data_interface.h"

namespace orderU {
	
	struct waiter_client : public data_interface
	{
	protected:
		std::map<user_permission, permission_type> _permissions;
		
	public:
		waiter_client(const std::string name, std::map<user_permission, permission_type> permissions = {}) : data_interface(
			name), _permissions(permissions)
		{

		}
		waiter_client() : data_interface()
		{

		}

		template <typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar& boost::serialization::base_object<data_interface>(*this);
			ar& _permissions;
		}

		bool hasPermission(user_permission up, permission_type pt)
		{
			return _permissions.at(up) & pt;
		}
	};

	class waiter : public data_interface
	{
	protected:
		int _code;
		std::map<user_permission, permission_type> _permissions;
		
	public:
		waiter(const std::string name, int _code, waiter_role _role = waiter_role::WAITER) : data_interface(
				name), _code(_code), _permissions(normal_permissions.at(_role))
		{
			
		}

		waiter() : data_interface(){}

		template <typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar & boost::serialization::base_object<data_interface>(*this);
			ar& _code;
			ar& _permissions;
		}

		int getCode() const {
			return _code;
		}

		void setCode(int code) {
			waiter::_code = code;
		}

		bool hasPermission(user_permission up, permission_type pt)
		{
			return _permissions.at(up) & pt;
		}

		waiter_client getClientVersion()
		{
			return waiter_client(getName(), _permissions);
		}
	};

} // namespace orderU

#endif // __WAITER_H__
//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __ORDER_H__
#define __ORDER_H__

#include <string>
#include <vector>
#include <memory>

#include "../object/item.h"
#include "../object/waiter.h"

namespace orderU {

	class order : public data_interface
	{
	protected:
		std::shared_ptr<item> _item;
		std::vector<std::shared_ptr<item>> subitems;
		std::shared_ptr<waiter> _waiter;
		int timestamp;
	public:

		order(std::shared_ptr<item> _i, std::shared_ptr<waiter> _w = 0, std::vector<std::shared_ptr<item>> subi = {})
			: data_interface(), _item(_i), _waiter(_w), timestamp(std::time(nullptr)), subitems(subi)
		{
			
		}

		order() : data_interface(){}
		~order(){}

		template <typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar & boost::serialization::base_object<data_interface>(*this);
			ar& _item;
			ar& _waiter;
			ar& subitems;
			ar& timestamp;
		}

		const std::shared_ptr<item> &getItem() const {
			return _item;
		}

		void setItem(const std::shared_ptr<item> &item) {
			_item = item;
		}

		const std::shared_ptr<waiter> &getWaiter() const {
			return _waiter;
		}

		void setWaiter(const std::shared_ptr<waiter> &waiter) {
			_waiter = waiter;
		}

		int getTimestamp() const {
			return timestamp;
		}

		void setTimestamp(int timestamp) {
			order::timestamp = timestamp;
		}


		const std::vector<std::shared_ptr<item>>& getSubitems() const {
			return subitems;
		}

		void setSubitems(const std::vector<std::shared_ptr<item>>& _subitems) {
			subitems = _subitems;
		}
	};

} // namespace orderU

#endif // __ORDER_H__
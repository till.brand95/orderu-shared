//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __BILL_H__
#define __BILL_H__

#include "data_interface.h"
#include "order.h"

namespace orderU {

	class bill : public data_interface
	{
	protected:
		int time = 0;
		int table = 0;
		std::vector<std::shared_ptr<order>> orders;

	public:
		bill(std::vector<std::shared_ptr<order>> _orders, int _table) : data_interface(), orders(_orders), table(_table) {
			time = std::time(nullptr);
		}

		bill() : data_interface() {}

		float getTotal()
        {
		    float total = 0.0f;
		    for(auto &o : orders)
            {
		        total += o->getItem()->getPrice();
            }
		    return total;
        }
		template <typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar & boost::serialization::base_object<data_interface>(*this);
			ar& time;
			ar& orders;
			ar& table;
		}

		const std::vector<std::shared_ptr <order>> &getOrders() const {
			return orders;
		}

		void addOrder(std::shared_ptr<order> order)
		{
			orders.push_back(order);
		}

		void removeOrder(int number) {

			orders.erase(orders.begin() + number);
		}
		void removeOrder(std::vector<std::shared_ptr<order>>::iterator it)  {

			orders.erase(it);
		}

		void removeOrder(std::shared_ptr<order> &o) {

			for (int i = 0; i < orders.size(); i++)
			{
				if (orders.at(i)->getId() == orders.at(i)->getId())
					removeOrder(i);
			}
		}

		bool hasOrder(const std::shared_ptr<order>& o) const
		{
			for (int i = 0; i < orders.size(); i++)
			{
				if (orders.at(i)->getId() == orders.at(i)->getId())
					return true;
			}
			return false;
		}

		bool hasOrder(const boost::uuids::uuid &uuid) const
		{
			for (int i = 0; i < orders.size(); i++)
			{
				if (orders.at(i)->getId() == uuid)
					return true;
			}
			return false;
		}

		void removeOrder(const boost::uuids::uuid& uuid)
		{
			for (int i = 0; i < orders.size(); i++)
			{
				if (orders.at(i)->getId() == uuid)
					removeOrder(i);
			}
		}

		int getTime() const {
			return time;
		}

		void setTime(int time) {
			bill::time = time;
		}

		int getTable() const {
			return table;
		}

		void setTable(int table) {
			bill::table = table;
		}
	};



} // namespace orderU

#endif // __TABLE_H__
//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __TABLE_H__
#define __TABLE_H__

#include "data_interface.h"
#include "../object/order.h"

namespace orderU {

	class table : public data_interface
	{
	protected:
		long number = 0;
		std::vector<std::shared_ptr<order>> orders;

	public:
		table(long _number) : data_interface(std::to_string(_number)), number(_number)
		{

		}

		table() : data_interface() {}

		template <typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar & boost::serialization::base_object<data_interface>(*this);
			ar& number;
			ar& orders;
		}

		const std::vector<std::shared_ptr <order>> &getOrders() const {
			return orders;
		}

		void addOrder(std::shared_ptr<order> order)
		{
			orders.push_back(order);
		}

		void addOrder(order _order)
		{
			orders.push_back(std::make_shared<order>(_order));
		}

		long getNumber() const {
			return number;
		}

		void setNumber(long number) {

			table::number = number;
		}

		void removeOrder(int number) {

			orders.erase(orders.begin() + number);
		}
		void removeOrder(std::vector<std::shared_ptr<order>>::iterator it)  {

			orders.erase(it);
		}

		void removeOrder(std::shared_ptr<order> &o) {

			for (int i = 0; i < orders.size(); i++)
			{
				if (orders.at(i)->getId() == orders.at(i)->getId())
					removeOrder(i);
			}
		}

		bool hasOrder(const std::shared_ptr<order>& o) const
		{
			for (int i = 0; i < orders.size(); i++)
			{
				if (orders.at(i)->getId() == orders.at(i)->getId())
					return true;
			}
			return false;
		}

		bool hasOrder(const boost::uuids::uuid &uuid) const
		{
			for (int i = 0; i < orders.size(); i++)
			{
				if (orders.at(i)->getId() == uuid)
					return true;
			}
			return false;
		}

		void removeOrder(const boost::uuids::uuid& uuid)
		{
			for (auto it = orders.begin(); it != orders.end(); it++)
			{
				if ((*it)->getId() == uuid)
				{
					orders.erase(it);
					return;
				}
			}
		}
	};

} // namespace orderU

#endif // __TABLE_H__
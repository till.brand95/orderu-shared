//
// Created by Till on 22/10/2019.
//

//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ORDERU_DATA_INTERFACE_H
#define ORDERU_DATA_INTERFACE_H

#include <string>
#include <vector>
#include <memory>

#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_serialize.hpp>

namespace orderU {

    /// Structure to hold information about a single stock.
class data_interface : public std::enable_shared_from_this<data_interface>
    {
    protected:
        boost::uuids::uuid _id = boost::uuids::random_generator()();
        std::string name = "";
    public:
        data_interface(std::string _name)
            : name(_name)
        {

        }
        data_interface(){}
        virtual ~data_interface(){}

    const std::string &getName() const {
        return name;
    }

    void setName(const std::string &name) {
        data_interface::name = name;
    }

    const boost::uuids::uuid &getId() const {
            return _id;
        }

        template <typename Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            ar& _id;
            ar& name;
        }
    };

} // namespace orderU

#endif // ORDERU_DATA_INTERFACE_H


#ifndef __ORDERU_SERVER_H__
#define __ORDERU_SERVER_H__

#include "orderu_shared.h"
#include "network/server.h"

namespace orderU
{
	class orderu_server : public orderu_shared<std::shared_ptr<server>>
	{
	public:
		orderu_server() : orderu_shared() 
		{
			_wm = std::make_shared<waiter_manager>();
			initData();
			_service = std::make_shared<server>(io_context, 1000);
			_service->addReceiveHandler([&](connection_ptr conn, const message& msg, message** answ) {
				receiveMessage(conn, msg, answ);
				});

			_service->onAccept = [&](connection_ptr conn, const boost::system::error_code& e) {
				if (e)
					return;

				_service->write(message(SMSG_HELLO), conn);
			};

			_im->onAdd.push_back([&](std::shared_ptr<item> i) {
				message msg(SMSG_ADD_ITEM, serializer::serialize(i));
				for (auto& c : logged_in_users)
				_service->write(msg, c.first, [](message ans){
				});
			});
			_tm->onAdd.push_back([&](std::shared_ptr<table> i) {
				message msg(SMSG_ADD_TABLE, serializer::serialize(i));
				for(auto &c : logged_in_users)
					_service->write(msg,c.first, [](message ans) {
				});
			});
			_bm->onAdd.push_back([&](std::shared_ptr<bill> i) {
				message msg(SMSG_ADD_BILL, serializer::serialize(i));
				for (auto& c : logged_in_users)
				_service->write(msg, c.first, [](message ans) {
				});
			});

			_im->onRemove.push_back([&](std::shared_ptr<item> i) {
				message msg(SMSG_REM_ITEM, serializer::serialize(i));
				for (auto& c : logged_in_users)
					_service->write(msg, c.first, [](message ans) {
						});
				});
			_tm->onRemove.push_back([&](std::shared_ptr<table> i) {
				message msg(SMSG_REM_TABLE, serializer::serialize(i));
				for (auto& c : logged_in_users)
					_service->write(msg, c.first, [](message ans) {
						});
				});
			_bm->onRemove.push_back([&](std::shared_ptr<bill> i) {
				message msg(SMSG_REM_BILL, serializer::serialize(i));
				for (auto& c : logged_in_users)
					_service->write(msg, c.first, [](message ans) {
						});
				});

			_im->onUpdate.push_back([&](std::shared_ptr<item> _i) {
				message msg(SMSG_UPD_ITEM, serializer::serialize(_i));
				for (auto& c : logged_in_users)
					_service->write(msg, c.first);
				});
			_tm->onUpdate.push_back([&](std::shared_ptr<table> _i) {
				message msg(SMSG_UPD_TABLE, serializer::serialize(_i));
				for (auto& c : logged_in_users)
					_service->write(msg, c.first);
				});
			_bm->onUpdate.push_back([&](std::shared_ptr<bill> _i) {
				message msg(SMSG_UPD_BILL, serializer::serialize(_i));
				for (auto& c : logged_in_users)
					_service->write(msg, c.first);
				});
			_service->onDisconnect.push_back([&](connection_ptr conn) {
				logout(conn);
				});

		}
		~orderu_server() {}

		void load_stored_data()
		{
			_wm->load();
			_im->load();
			_tm->load();
			_bm->load();
			}

		void setWaiterManager(std::shared_ptr<waiter_manager> in) { _wm = in; }

		void receiveMessage(connection_ptr conn, const orderU::message& _message, orderU::message** answer);

		auto& getWaiterManager() { return _wm; }

		void addWaiter(std::string name, int code)
		{
			waiter _w(name, code);
			_wm->add(_w);
		}

		bool haveWaiterByCode(int code)
		{
			for (auto& waiter : _wm->get_list())
			{
				if (waiter->getCode() == code)
					return true;
			}
			return false;
		}

		bool login(connection_ptr conn, int code)
		{
			if (loggedIn(conn))
				return true;
			for (auto& w : _wm->get_list())
			{
				if (w->getCode() == code)
				{
					logged_in_users[conn] = w;
					return true;
				}
			}
			return false;
		}

		void logout(connection_ptr conn)
		{
			if (!loggedIn(conn))
				return;
			logged_in_users.erase(conn);
			
		}

		bool loggedIn(connection_ptr conn)
		{
			return logged_in_users.count(conn) > 0;
		}

		std::shared_ptr<waiter> getUser(connection_ptr conn)
		{
			if (loggedIn(conn))
				return logged_in_users.at(conn);
			return 0;
		}

	protected:
		std::map<connection_ptr, std::shared_ptr<waiter>> logged_in_users;
		std::shared_ptr<waiter_manager> _wm;

		
		

		void initData();
	

	};
}


#endif
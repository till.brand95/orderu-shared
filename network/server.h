#include <sstream>
#include <iostream>
#include <queue>
#include <boost/asio.hpp>


#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <vector>
#include "connection.h" // Must come before boost/serialization headers.

#include "message.h"
#include "service.h"

#ifndef __SERVER_H__
#define __SERVER_H__

namespace orderU {

	typedef std::function<void(connection_ptr, const boost::system::error_code &)> accept_handler;
	/// Serves stock quote information to any client that connects to it.
	class server : public service
	{
	public:
		/// Constructor opens the acceptor and starts waiting for the first incoming
		/// connection.
		server(boost::asio::io_context& io_context, unsigned short port)
			: acceptor_(io_context,
				boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)), service()
		{
			// Create the data to be sent to each client.			

			// Start an accept operation for a new connection.

			do_listen();
			
			
		}

	private:
		void do_listen()
		{
			connection_ptr new_conn(new connection(acceptor_.get_executor()));

			std::cout << "Waiting for connection" << std::endl;
			acceptor_.async_accept(*new_conn->socket().get(),
				boost::bind(&server::handle_accept, this,
					boost::asio::placeholders::error, new_conn));
			
		}
		
		void handle_accept(const boost::system::error_code& e, connection_ptr conn)
		{
			if ((boost::asio::error::eof == e) ||
				(boost::asio::error::connection_reset == e))
			{
				for (auto handler : onDisconnect)
					handler(conn);
					return;
			}
			if (!e)
			{
				if(onAccept)
				{
					onAccept(conn, e);
				}
					
				do_listen();	
				do_read(conn);
				if (!conn->write_msgs_.empty())
					do_write(conn);
			}
			else
			{
				close(conn);
			}
		}

		void reset()
		{

		}


	public:
		accept_handler onAccept;
		
	private:
		boost::asio::ip::tcp::acceptor acceptor_;
		

	};

} // namespace s11n_example

#endif
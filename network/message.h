//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#include <limits>
#include <string>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_serialize.hpp>

namespace orderU {

	enum MessageType : uint64_t
	{

		//CLIENT
		CMSG_LOGIN = 201,
		CMSG_QUERY_ORDER_LIST = 202,
		CMSG_QUERY_TABLE_LIST = 203,
		CMSG_QUERY_ITEM_LIST = 204,
		CMSG_NEW_ORDER = 205,
		CMSG_QUERY_ORDER_BY_TABLE = 206,
		CMSG_ORDER_PAY = 207,
		CMSG_QUERY_BILLS = 208,
		CMSG_NEW_ITEM = 209,


		//Server
		SMSG_LOGIN_FAILED = 305,
		SMSG_LOGIN_SUCCESS = 306,

		SMSG_ORDER_LIST = 307,
		SMSG_NEW_ORDER = 308,

		SMSG_TABLE_LIST = 309,
		SMSG_ITEM_LIST = 310,
		SMSG_ADDED_ORDER = 311,
		SMSG_ORDER_PAY_OK = 312,
		SMSG_BILLS = 313,

		SMSG_ADD_ITEM = 314,
		SMSG_ADD_BILL = 315,
		SMSG_ADD_TABLE = 316,
		SMSG_REM_ITEM = 317,
		SMSG_REM_BILL = 318,
		SMSG_REM_TABLE = 319,
		SMSG_UPD_ITEM = 320,
		SMSG_UPD_BILL = 321,
		SMSG_UPD_TABLE = 322,
		SMSG_WAITER,

		FAILED = 100,
		CONNECTED = 101,
		CMSG_HELLO = 102,
		SMSG_HELLO = 103

	};

	const std::map<MessageType, const char*> MessageTypeStrings{
			{CMSG_LOGIN,            "CMSG_LOGIN"},
			{CMSG_QUERY_ORDER_LIST, "CMSG_QUERY_ORDER_LIST"},
			{CMSG_QUERY_TABLE_LIST, "CMSG_QUERY_TABLE_LIST"},
			{CMSG_QUERY_ITEM_LIST,  "CMSG_QUERY_ITEM_LIST"},
			{SMSG_LOGIN_FAILED,     "SMSG_LOGIN_FAILED"},
			{SMSG_LOGIN_SUCCESS,    "SMSG_LOGIN_SUCCESS"},
			{SMSG_ORDER_LIST,       "SMSG_ORDER_LIST"},
			{SMSG_NEW_ORDER,        "SMSG_NEW_ORDER"},
			{SMSG_TABLE_LIST,       "SMSG_TABLE_LIST"},
			{SMSG_ITEM_LIST,        "SMSG_ITEM_LIST"},
			{FAILED,                "FAILED"},
			{CONNECTED,             "CONNECTED"},
			{CMSG_HELLO,             "CMSG_HELLO"},
			{SMSG_HELLO,             "SMSG_HELLO"},
			{SMSG_ADDED_ORDER,             "SMSG_ADDED_ORDER"},
			{CMSG_NEW_ORDER,             "CMSG_NEW_ORDER"},
			{CMSG_QUERY_ORDER_BY_TABLE,             "CMSG_QUERY_ORDER_BY_TABLE"},
			{CMSG_ORDER_PAY, "CMSG_ORDER_PAY"},
			{SMSG_ORDER_PAY_OK, "SMSG_ORDER_PAY_OK"},
			{CMSG_QUERY_BILLS, "CMSG_QUERY_BILLS"},
			{SMSG_BILLS, "SMSG_BILLS"},
			{SMSG_ADD_ITEM, "SMSG_ADD_ITEM"},
			{SMSG_ADD_BILL, "SMSG_ADD_BILL"},
			{SMSG_ADD_TABLE, "SMSG_ADD_TABLE"},
			{SMSG_REM_ITEM, "SMSG_REM_ITEM"},
			{SMSG_REM_BILL, "SMSG_REM_BILL"},
			{SMSG_REM_TABLE, "SMSG_REM_TABLE"},
			{SMSG_UPD_ITEM, "SMSG_UPD_ITEM"},
			{SMSG_UPD_BILL, "SMSG_UPD_BILL"},
			{SMSG_UPD_TABLE, "SMSG_UPD_TABLE"},
			{CMSG_NEW_ITEM, "CMSG_NEW_ITEM"},
			{SMSG_WAITER,"SMSG_WAITER"}
	};

	class message
	{
	public:
		boost::uuids::uuid uuid = boost::uuids::random_generator()();
		MessageType id;
		std::string data = "";
		std::string answer_of = "";
		std::time_t time = std::time(nullptr);

		message(MessageType _id, std::string _data, std::string answer = ""):id(_id),data(_data), answer_of(answer){}
		message(MessageType _id):id(_id){}
		message(){}

		template <typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar& uuid;
			ar& id;
			ar& data;
			ar& answer_of;
		}
	};

	struct MessageCompare
	{
		bool operator()(const message& lhs, const message& rhs)
		{
			return lhs.time < rhs.time;
		}
	};

} // namespace orderU

#endif // __ORDER_H__
//
// Created by Till on 14/10/2019.
//

#ifndef  __CLIENT_H__
#define  __CLIENT_H__

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include "connection.h"
#include <atomic>
#include <iostream>
#include <queue>
#include <vector>
#include <functional>
#include <shared_mutex>
#include <mutex>

#include "message.h"

#include "service.h"




#define APPNAME "orderU"


namespace orderU
{

	class client : public service
	{
	public:
		std::function<void()> onMessageHello;
		client() : service() {

        }


		void init(boost::asio::io_context& io_context,
				  const char* _host, const char * _service)
		{
			connection_ = boost::make_shared<connection>(io_context.get_executor());
			boost::asio::ip::tcp::resolver resolver(io_context);
			auto endpoint_iterator = resolver.resolve(_host, _service);
			do_connect(endpoint_iterator);
		}

		void close()
		{
			service::close(connection_);
		}

		void write(const message& msg, std::function<void(message)> handler = nullptr)
		{
			service::write(msg, connection_,handler);
		}

        void reset(boost::asio::io_context& io_context,
                   const char* _host, const char * _service)
        {
            connection_->reset(io_context.get_executor());
            boost::asio::ip::tcp::resolver resolver(io_context);
            auto endpoint_iterator = resolver.resolve(_host, _service);
            do_connect(endpoint_iterator);
        }

	private:
		void do_connect(const boost::asio::ip::tcp::resolver::results_type& endpoints)
		{
			LOG("%s", "DO_CONNECT");
			boost::asio::async_connect(*connection_->socket().get(), endpoints,
				boost::bind(&client::handle_connect, this,
					boost::asio::placeholders::error));
		}

		void handle_connect(const boost::system::error_code& e)
		{
			if (e)
			{
				LOG("HAVE ERROR: %s", e.message().c_str());
			}
			if ((boost::asio::error::eof == e) ||
				(boost::asio::error::connection_reset == e))
			{
				for (auto handler : onDisconnect)
					handler(connection_);
				return;
			}
			if (!e)
			{
				for (auto handler : onConnect)
				{
					handler(e);

				}
				do_read(connection_);
			}
		}

	public:
		std::vector<std::function<void(const boost::system::error_code & e)>> onConnect;


	private:
		connection_ptr connection_;


	};
}

#endif //MY_APPLICATION_CLIENT_H

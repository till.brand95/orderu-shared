#include <sstream>
#include <iostream>
#include <queue>
#include <boost/asio.hpp>


#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <vector>
#include "connection.h" // Must come before boost/serialization headers.

#include "message.h"

#ifndef __SERVICE_H__
#define __SERVICE_H__

#ifdef __ANDROID_API__
#include <android/log.h>
#endif
#ifdef __ANDROID_API__
#define LOG(_LOG_, ...) __android_log_print(ANDROID_LOG_VERBOSE, (const char *)"OrderU", _LOG_, ##__VA_ARGS__);
#else
#define linebreak(__MSG__) std::string(std::string(__MSG__) + "\n").c_str()
#define LOG(_LOG_, ...) printf(linebreak(_LOG_), ##__VA_ARGS__);
#endif



namespace orderU {

	typedef std::vector<std::function<void(connection_ptr, const message&, message**)>> function_handler;

	/// Serves stock quote information to any client that connects to it.
	class service
	{
	public:
		/// Constructor opens the acceptor and starts waiting for the first incoming
		/// connection.
		service() 
		{
			// Create the data to be sent to each client.			

			// Start an accept operation for a new connection.			
			
		}

		void close(connection_ptr conn)
		{
			boost::asio::post(conn->socket()->get_executor(), [&]() { conn->socket()->close(); });
		}

		void write(const message& msg, connection_ptr conn, std::function<void(message)> handler = nullptr)
		{
			boost::asio::post(conn->socket()->get_executor(),
				[this, msg, handler, conn]()
				{
					bool have_to_do_write = conn->write_msgs_.empty();
					conn->write_msgs_.push(msg);
					if (handler)
					{
						std::unique_lock<std::mutex> lock(mtx);
						conn->message_handlers.emplace((char*)msg.uuid.data, handler);
					}

					if (have_to_do_write)
						do_write(conn);
				});
		}

		void addReceiveHandler(std::function<void(connection_ptr, const message&, message**)> handler)
		{
			if(handler)
				onReceiveHandler.push_back(handler);
		}

	protected:

		void do_read(connection_ptr conn)
		{
			LOG ("%s", "WAITING FOR MESSAGE");
			conn->async_read(conn->read_msg_,
				boost::bind(&service::handle_read, this,
					boost::asio::placeholders::error, conn)
			);
		}

		void handle_read(const boost::system::error_code& ec, connection_ptr conn)
		{
			if (ec)
			{
				LOG("HAVE ERROR: %s", ec.message().c_str());
			}
			if ((boost::asio::error::eof == ec) ||
				(boost::asio::error::connection_reset == ec))
			{
				for (auto handler : onDisconnect)
					handler(conn);
					return;
			}

			if (!ec)
			{
				LOG ("%s: %s", "NEW MESSAGE ID: ", MessageTypeStrings.at(conn->read_msg_.id));


				message *_new_message = nullptr;
				
				for (auto& handler : onReceiveHandler)
					handler(conn, conn->read_msg_, &_new_message);

				if (_new_message)
				{
					_new_message->answer_of = (char*)conn->read_msg_.uuid.data;
					write(*_new_message, conn, [](message) {});
					delete _new_message;
				}

				{
					std::unique_lock<std::mutex> lock(mtx);
					if (conn->message_handlers.count(conn->read_msg_.answer_of) > 0 && conn->message_handlers.at(conn->read_msg_.answer_of)) {
						LOG("CALLING_RCV_HANDLER");
						conn->message_handlers.at(conn->read_msg_.answer_of)(conn->read_msg_);
						conn->message_handlers.erase(conn->read_msg_.answer_of);
					}
					else
						LOG("NOHANDLER GIVEN");
				}

				do_read(conn);
			}
			else
			{
				close(conn);
			}
		}

		void do_write(connection_ptr conn)
		{
			LOG ("SENDING MESSAGE ID: %s", (MessageTypeStrings.count(conn->write_msgs_.front().id) == 1 ? MessageTypeStrings.at(conn->write_msgs_.front().id) : std::to_string(conn->write_msgs_.front().id).c_str()));
			conn->async_write(conn->write_msgs_.front(),
				boost::bind(&service::handle_write, this,
					boost::asio::placeholders::error, conn));
		}
		void handle_write(const boost::system::error_code& ec, connection_ptr conn)
		{
			LOG("%s: %s", "HANDLE_WRITE: ", MessageTypeStrings.at(conn->write_msgs_.front().id));
			if (ec)
			{
				LOG("HAVE ERROR: %s", ec.message().c_str());
			}
			if ((boost::asio::error::eof == ec) ||
				(boost::asio::error::connection_reset == ec))
			{
				for (auto handler : onDisconnect)
					handler(conn);
				return;
			}
			if (!ec)
			{
				conn->write_msgs_.pop();
				if (!conn->write_msgs_.empty())
				{
					do_write(conn);
				}
			}
			else
			{
				close(conn);
			}
		}

		public:
		std::vector<std::function<void(connection_ptr)>> onDisconnect;
	protected:
		function_handler onReceiveHandler;


		std::mutex mtx;

	};

} // namespace s11n_example

#endif
#ifndef __ORDERU_CLIENT_H__
#define __ORDERU_CLIENT_H__


#include "orderu_shared.h"


namespace orderU
{
	class orderu_client : public orderu_shared<client*>
	{
	public:
		orderu_client() :
			orderu_shared()
		{
			_service = new client();
		}

		void create(const char*ip)
		{

			_service->init(io_context, ip, "1000");
			_service->addReceiveHandler([&](connection_ptr conn, const message& msg, message** answ) {
				LOG("RECEIVE HANDLER");
				receiveMessage(conn, msg, answ);
			});
		}

		void reset(const char*ip)
		{
			if(!io_context.stopped())
				io_context.stop();
			io_context.reset();
			_service->reset(io_context, ip, "1000");
			run();
		}

		bool login(int code)
		{
			std::atomic<bool> has_result;
			std::atomic<bool> result;
			result.store(false);
			has_result.store(false);
			_service->write(message(CMSG_LOGIN, std::to_string(code)), [&](const message& ans) {

				if (ans.id == SMSG_LOGIN_SUCCESS)
				{
					result.store(true);
				}
				has_result.store(true);
				});

			while (!has_result.load())
			{
			}
			return result;
		}

		void addOrderList(std::vector<order> o, int _table)
		{
			table t(_table);
			for(auto &ord : o)
				t.addOrder(ord);
			_service->write(message(CMSG_NEW_ORDER, serializer::serialize(t)));
		}
		void addOrder(order b, int table)
		{
			addOrderList({ b }, table);
		}
		void addItemList(std::vector<item> i)
		{
			_service->write(message(CMSG_NEW_ITEM, serializer::serialize(i)));
		}
		void addItem(item b)
		{
			addItemList({ b });
		}

		void addBillList(std::vector<bill> i)
		{
			_service->write(message(CMSG_ORDER_PAY, serializer::serialize(i)));
		}

		void addBill(bill b)
		{
			addBillList({ b });
		}


		~orderu_client() {}
		void receiveMessage(connection_ptr conn, const orderU::message& _message, orderU::message** answer);

		auto getWaiter() { return _waiter; }

		waiter_client _waiter;
	};
}


#endif
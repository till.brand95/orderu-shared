#ifndef __ORDERU_SHARED_H__
#define __ORDERU_SHARED_H__


#include <boost/thread.hpp>
#include "network/client.h"
#include "network/server.h"
#include "network/connection.h"
#include "network/message.h"

#include "manager/table_manager.h"
#include "manager/item_manager.h"
#include "manager/order_manager.h"
#include "manager/waiter_manager.h"
#include "manager/bill_manager.h"


namespace orderU
{
	template<typename T>
	class orderu_shared
	{
	public:
		orderu_shared() {
			_im = std::make_shared<item_manager>();
			_tm = std::make_shared<table_manager>();
			_bm = std::make_shared<bill_manager>();
		}
		virtual ~orderu_shared() {}


		auto& getItemManager() { return _im; }
		auto& getTableManager() { return _tm; }
		auto& getBillManager() { return _bm; }

		void setItemManager(std::shared_ptr<item_manager> in) { _im = in; }
		void setTableManager(std::shared_ptr<table_manager> in) { _tm = in; }
		void setBillManager(std::shared_ptr<bill_manager> in) { _bm = in; }

		void addOrder(std::shared_ptr<item> _item, std::shared_ptr < waiter> _waiter, std::shared_ptr < table> _table)
		{
			std::shared_ptr<order> _order = std::make_shared<order>(_item, _waiter);
			_table->addOrder(_order);
		}

		void addTable(int number)
		{
			table t(number);
			_tm->add(t);
		}

		void addItem(item _item)
		{
			_im->add(_item);
		}

		void addItem(std::string name, float price)
		{
			item _i(name, price);

			_im->add(_i);
		}

		T getService() { return _service; }
		boost::asio::io_context& getIoContext() { return io_context; }

		std::function<void(connection_ptr, const message&, orderU::message**)> onReceive;
		std::function<void(connection_ptr, const message&, orderU::message**)> onSend;

		virtual void receiveMessage(connection_ptr conn, const orderU::message& _message, orderU::message** answer) = 0;

		void run()
		{
			boost::thread t{ [&]() {io_context.run(); } };
		}

		void reset()
		{
			_service->reset();
			run();
		}

		void stop()
		{
			io_context.stop();
		}

		bool stopped() { return io_context.stopped(); }

	protected:

		std::shared_ptr < item_manager> _im;
		std::shared_ptr < table_manager> _tm;
		std::shared_ptr < bill_manager> _bm;
		boost::asio::io_context io_context;
		T _service;

		std::mutex mtx;
	};

}


#endif
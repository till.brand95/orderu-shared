
#ifndef __SERIALIZER_H__
#define __SERIALIZER_H__

#include <sstream>
#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/uuid/uuid_serialize.hpp>

namespace orderU
{
	class serializer
	{
	public:
		template <typename T>
		static T deserialize(std::string serialized)
		{
			std::stringstream ifs(serialized);
			T ret;

			// create class instance
			{
				boost::archive::text_iarchive ia(ifs);
				// write class instance to archive
				ia >> ret;
				// archive and stream closed when destructors are called
			}
			return ret;
		}

		template <typename T>
		static std::string serialize(T object)
		{
			std::stringstream ofs;
			// create class instance
			{
				boost::archive::text_oarchive oa(ofs);
				// write class instance to archive
				oa << object;
				// archive and stream closed when destructors are called
			}
			return ofs.str();
		}

		template <typename T>
		static T deserializeFromFile(std::string file)
		{
			std::ifstream ifs(file);
			T ret;

			// create class instance
			{
				boost::archive::text_iarchive ia(ifs);
				// write class instance to archive
				ia >> ret;
				// archive and stream closed when destructors are called
			}
			return ret;
		}

		template <typename T>
		static void serializeToFile(T object, std::string file)
		{
			std::ofstream ofs(file);
			// create class instance
			{
				boost::archive::text_oarchive oa(ofs);
				// write class instance to archive
				oa << object;
				// archive and stream closed when destructors are called
			}

		}
	};
}
#endif
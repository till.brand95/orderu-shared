//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __ITEM_MANAGER_H__
#define __ITEM_MANAGER_H__

#include <string>
#include <vector>
#include <memory>

#include "abstract_manager.h"

#include "../object/item.h"

namespace orderU {
	class item_category_manager : public abstract_manager<item_category>
	{
	public:
		item_category_manager() {

		}

		void store() { abstract_manager::store("categories"); }
		void load() { abstract_manager::load("categories"); }
	};
	class item_manager : public abstract_manager<item>
	{
		public:
		item_manager(){
			icm = std::make_shared< item_category_manager>();
		}
		~item_manager(){
		}

		std::shared_ptr<item_category_manager> icm;

		auto& getItemCategorieManager() { return icm; }

		void add(item i, std::string category)
		{
			add(std::make_shared<item>(i), category);
		}

		void add(item i)
		{
			add(std::make_shared<item>(i));
		}

		void add(std::shared_ptr<item> i)
		{
			
			if (i->getCategory())
			{
				bool found = false;
				for (auto& cat : icm->get_list())
				{
					if (cat->getName() == i->getCategory()->getName())
						found = true;
				}
				if (!found)
				{
					icm->add(i->getCategory());
				}
			}
			abstract_manager::add(i);
		}

		void add(std::shared_ptr<item> i, std::string category)
		{
			if (!i->getCategory())
			{
				for (auto& cat : icm->get_list())
				{
					if (cat->getName() == category)
						i->setCategory(cat);
				}
			}
			abstract_manager::add(i);
		}

		void store() { icm->store(); abstract_manager::store("items"); }
		void load() { icm->load(); abstract_manager::load("items"); }
	};

} // namespace orderU

#endif // __ITEM_MANAGER_H__
//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __BILL_MANAGER_H__
#define __BILL_MANAGER_H__

#include <string>
#include <vector>
#include <memory>

#include "abstract_manager.h"

#include "../object/bill.h"


namespace orderU {

	class bill_manager : public abstract_manager<bill>
	{
		public:
		bill_manager(){
	
		}
		~bill_manager(){}

		void store() { abstract_manager::store("bills"); }
		void load() { abstract_manager::load("bills"); }
	};

} // namespace orderU

#endif // __ORDER_MANAGER_H__
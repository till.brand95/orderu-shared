#pragma once

#ifndef __ABSTRACT_MANAGER_H
#define __ABSTRACT_MANAGER_H

#include <vector>
#include <iterator>
#include <memory>

#include "../object/data_interface.h"
#include "../global/serializer.h"

namespace orderU {

	template<typename T, typename std::enable_if<std::is_base_of<data_interface, T>::value>::type * = nullptr >
	class abstract_manager
	{
	public:
		abstract_manager() {}
		virtual ~abstract_manager(){}

		void add(T _item)
		{
			add(std::make_shared<T>(_item));
		}

		void add(std::shared_ptr<T> _item)
		{
			_items.push_back(_item);
			for (auto handler : onAdd)
				handler(_item);
		}

		void add(T *_item)
		{
			add(std::make_shared<T>(_item));
		}

		void remove(int i)
		{
			remove(_items.begin() + i);
		}

		void remove(typename std::vector<std::shared_ptr<T>>::iterator _it)
		{
			for (auto handler : onRemove)
				handler(*_it);
			_items.erase(_it);
		}

		std::shared_ptr<T>& get(int i)
		{
			return _items.at(i);
		}

		std::vector<std::shared_ptr<T>>& get_list()
		{
			return _items;
		}

		void set_list(std::vector<std::shared_ptr<T>> list)
		{
			_items = list;
			for (auto& i : _items)
				for (auto handler : onAdd)
					handler(i);
		}

		template <typename Archive>
		void serialize(Archive& ar, const unsigned int version)
		{
			ar& _items;
		}

		bool hasItemById(const boost::uuids::uuid& uuid) const
		{
			for (auto& i : _items)
			{
				if (i->getId() == uuid)
					return true;
			}
			return false;
		}

		bool removeItemById(const boost::uuids::uuid& uuid)
		{
			for (int i = 0; i < _items.size(); i++)
			{
				if (_items.at(i)->getId() == uuid)
				{
					remove(i);
					break;
				}
			}
			return false;
		}
		std::shared_ptr<T> getItemById(const boost::uuids::uuid& uuid) const
		{
			for (auto& i : _items)
			{
				if (i->getId() == uuid)
					return i;
			}
		}

		void update(std::shared_ptr<T> item)
		{
			for (auto it  = _items.begin(); it != _items.end(); it++)
			{
				if ((*it)->getId() == item->getId())
				{
					for (auto& handler : onUpdate)
						handler(item);
					*it = item;
				}
			}
			
		}

		std::vector<std::function<void(std::shared_ptr<T>&)>>  onAdd;
		std::vector < std::function<void(std::shared_ptr<T>&)>> onRemove;
		std::vector < std::function<void(std::shared_ptr<T>&)>> onUpdate;

	protected:
		void store(std::string file)
		{
			serializer::serializeToFile(_items, file);
		}
		void load(std::string file)
		{
			_items = serializer::deserializeFromFile<std::vector<std::shared_ptr<T>>>(file);
			for (auto& i : _items)
				for (auto handler : onAdd)
					handler(i);
		}


		


	protected:
		std::vector<std::shared_ptr<T>> _items;
		


	};
}

#endif

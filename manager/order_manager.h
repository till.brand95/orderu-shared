//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __ORDER_MANAGER_H__
#define __ORDER_MANAGER_H__

#include <string>
#include <vector>
#include <memory>

#include "abstract_manager.h"

#include "../object/order.h"


namespace orderU {

	class order_manager : public abstract_manager<order>
	{
		public:
		order_manager(){
	
		}
		~order_manager(){}

		void store() { abstract_manager::store("orders"); }
		void load() { abstract_manager::load("orders"); }
	};

} // namespace orderU

#endif // __ORDER_MANAGER_H__
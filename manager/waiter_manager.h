//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __WAITER_MANAGER_H__
#define __WAITER_MANAGER_H__

#include <string>
#include <vector>
#include <memory>

#include "abstract_manager.h"
#include "../object/waiter.h"

namespace orderU {

	class waiter_manager : public abstract_manager<waiter>
	{
		
		public:
		waiter_manager(){
		}
		~waiter_manager(){}

		void store() { abstract_manager::store("waiters"); }
		void load() { abstract_manager::load("waiters"); }
		

		bool hasWaiterWithCode(int code) const
		{
			for (auto& w : _items)
			{
				if (w->getCode() == code)
					return true;
			}
			return false;
		}

		std::shared_ptr<waiter> getWaiterWithCode(int code)
		{
			std::shared_ptr<waiter> ret = 0;
			for (auto& w : _items)
			{
				if (w->getCode() == code)
					ret = w;
			}
			return ret;
		}
	};

} // namespace orderU

#endif // __ITEM_MANAGER_H__
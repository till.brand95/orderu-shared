//
// stock.hpp
// ~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef __T_MANAGER_H__
#define __T_MANAGER_H__

#include "abstract_manager.h"
#include "../object/table.h"

namespace orderU {

	class table_manager : public abstract_manager<table>
	{
		public:
			table_manager():abstract_manager() {
	
		}
		~table_manager(){}

		void store() { abstract_manager::store("tables"); }
		void load() { abstract_manager::load("tables"); }

		bool hasTableByNumber(int number) const
		{
				for(auto &t : _items)
					if(t->getNumber() == number)
						return true;
					return false;
		}

		std::shared_ptr<table> getTableByNumber(int number)
		{
			for(auto &t : _items)
				if(t->getNumber() == number)
					return t;
			return 0;
		}
	};

} // namespace orderU

#endif // __ORDER_MANAGER_H__
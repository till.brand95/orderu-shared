#include "orderu_server.h"
#include "global/serializer.h"

void orderU::orderu_server::receiveMessage(connection_ptr conn, const orderU::message& _message, orderU::message** answer)
{
	if (onReceive)
		onReceive(conn, _message, answer);
	switch (_message.id)
	{
	case MessageType::CMSG_QUERY_TABLE_LIST:
	{
		*answer = new message();
		(*answer)->id = MessageType::SMSG_TABLE_LIST;
		(*answer)->data = serializer::serialize(getTableManager()->get_list());
		break;
	}
	case MessageType::CMSG_QUERY_ITEM_LIST:
	{
		*answer = new message();
		(*answer)->id = MessageType::SMSG_ITEM_LIST;
		(*answer)->data = serializer::serialize(getItemManager()->get_list());
		break;
	}
	case MessageType::CMSG_QUERY_BILLS:
	{
		int count = 0;
		try {
			count = std::stoi(_message.data);
		}
		catch (...)
		{
		}
		std::vector<std::shared_ptr<bill>> bills;
		int size = getBillManager()->get_list().size() - 1;
		int to = size >= count ? size - count : 0;
		for (int i = size; i >= to; i--)
		{
			bills.push_back(getBillManager()->get(i));
		}
		(*answer) = new message();
		(*answer)->id = MessageType::SMSG_BILLS;
		(*answer)->data = serializer::serialize<std::vector<std::shared_ptr<bill>>>(bills);
		break;
	}
	case MessageType::CMSG_QUERY_ORDER_BY_TABLE:
	{
		(*answer) = new message();
		(*answer)->id = MessageType::SMSG_ORDER_LIST;
		for (auto _table : getTableManager()->get_list())
		{
			if (std::stoi(_message.data) == _table->getNumber())
			{
				std::vector<std::shared_ptr<table>> tables;
				tables.push_back(_table);
				(*answer)->data = serializer::serialize(tables);
				break;
			}
		}

		break;
	}
	case MessageType::CMSG_ORDER_PAY:
	{
		bool found = false;
		auto bills = serializer::deserialize<std::vector<bill>>(_message.data);
		if (bills.size() == 0)
			break;
		
		for (auto& _bill : bills)
		{
			int count = 0;
			auto table = getTableManager()->getTableByNumber(_bill.getTable());
			if (!table)
				continue;
			auto currorders = table->getOrders();
			std::vector<std::shared_ptr<order>> bill_orders;
			for (auto& table_order : currorders)
			{
				for (auto& o : _bill.getOrders())
				{
					if (o->getId() == table_order->getId())
					{
						table->removeOrder(table_order->getId());
						bill_orders.push_back(table_order);
					}
						
				}
			}
			if (table->getOrders().size() == 0)
				getTableManager()->removeItemById(table->getId());
			else
				_tm->update(table);
			if (bill_orders.size() == _bill.getOrders().size())
			{

				std::shared_ptr<bill> bill_ = std::make_shared<bill>(bill_orders, _bill.getTable());
				getBillManager()->add(bill_);
			}
		}
		break;

	}
	case MessageType::CMSG_LOGIN:
	{
		int code = std::stoi(_message.data);
		*answer = new message();
		(*answer)->data = "";
		if (login(conn, code))
		{
			_service->write(message(SMSG_ITEM_LIST, serializer::serialize(getItemManager()->get_list())), conn);
			_service->write(message(SMSG_TABLE_LIST, serializer::serialize(getTableManager()->get_list())), conn);
			_service->write(message(SMSG_BILLS, serializer::serialize(getBillManager()->get_list())), conn);
			auto wc = logged_in_users.at(conn)->getClientVersion();
			_service->write(message(SMSG_WAITER, serializer::serialize(wc)), conn);
			
			(*answer)->id = MessageType::SMSG_LOGIN_SUCCESS;
		}
		else
			(*answer)->id = MessageType::SMSG_LOGIN_FAILED;
		break;
	}
	case MessageType::CMSG_NEW_ITEM:
	{
		std::vector<item> prop = serializer::deserialize<std::vector<item>>(_message.data);

		for (auto &i: prop)
		{
			std::shared_ptr<item> it = std::make_shared<item>(i.getName(), i.getPrice());
			getItemManager()->add(it);
		}
		break;
	}
	case MessageType::CMSG_NEW_ORDER:
	{
		if (!loggedIn(conn))
			break;
		table _table = serializer::deserialize<table>(_message.data);

		std::shared_ptr<waiter> _waiter = getUser(conn);
		if (!_waiter)
			break;

		std::shared_ptr<table> table1 = getTableManager()->getTableByNumber(_table.getNumber());
		if (!table1)
		{
			table1 = std::make_shared<table>(_table.getNumber());
			getTableManager()->add(table1);
		}
		if (!table1)
			break;
		for (auto& _order : _table.getOrders())
		{
			if (getItemManager()->hasItemById(_order->getItem()->getId()))
			{
				std::shared_ptr<order> o = std::make_shared<order>(_order->getItem(), _waiter);
				table1->addOrder(o);
			}
		}
		_tm->update(table1);
		break;
	}
	default:
		break;
	}
}

void orderU::orderu_server::initData()
{

	item tofu("Tofu", 4.56);
	item wasser("Wasser", 0.25);
	item schnitzel("Schnitzel mit Pommes und Salat", 14.80);

	_im->add(tofu);
	_im->add(wasser);
	_im->add(schnitzel);

	waiter _waiter("Till", 1234);
	_wm->add(_waiter);

	table t(100);
	table t1(200);



	std::shared_ptr<order> _order = std::make_shared<order>(_im->get(0), _wm->get(0));
	std::shared_ptr<order> _order1 = std::make_shared < order>(_im->get(1), _wm->get(0));
	std::shared_ptr<order> _order2 = std::make_shared<order>(_im->get(2), _wm->get(0));
	std::shared_ptr<order> _order3 = std::make_shared<order>(_im->get(0), _wm->get(0));
	std::shared_ptr<order> _order4 = std::make_shared<order>(_im->get(1), _wm->get(0));
	std::shared_ptr<order> _order5 = std::make_shared<order>(_im->get(2), _wm->get(0));

	t.addOrder(_order); t.addOrder(_order1); t.addOrder(_order2);
	t1.addOrder(_order3); t1.addOrder(_order4); t1.addOrder(_order5);

	_tm->add(t); _tm->add(t1);

	std::shared_ptr<order> _order11 = std::make_shared<order>(_im->get(0), _wm->get(0));
	std::shared_ptr<bill> _bill = std::make_shared<bill>(bill({ _order11 }, 100));
	_bm->add(_bill);

	_im->store();
	_wm->store();
	_tm->store();
	_bm->store();
}